#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "GL/freeglut.h"
#include "GL/gl.h"
#include "libbase64.h"

#define ARRSIZE(x) (sizeof(x)/sizeof((x)[0]))

enum {
	INPUT_TYPE_MONO,
	INPUT_TYPE_RGB8,
	INPUT_TYPE_COUNT
};

int mouseState = 0;
int mouseX = 0,
	mouseY = 0;
int windowWidth = 500,
	windowHeight = 500;

/* bytes per pixel for each type */
int type_bpp[INPUT_TYPE_COUNT] = {
	[INPUT_TYPE_MONO] = 1,
	[INPUT_TYPE_RGB8] = 3
};

int type_glt[INPUT_TYPE_COUNT] = {
	[INPUT_TYPE_MONO] = GL_LUMINANCE,
	[INPUT_TYPE_RGB8] = GL_RGB
};

GLuint tex;

void
die(const char *fmtstr, ...)
{
	va_list ap;

	va_start(ap, fmtstr);
	vfprintf(stderr, fmtstr, ap);
	va_end(ap);

	exit(EXIT_FAILURE);
}

void
timer(int a)
{
	glutPostRedisplay();
	glutTimerFunc(1000/60.0, timer, 0);
}

void
initGL()
{
	glClearColor(0.0, 0.0, 0.0, 1.0);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	glOrtho(0.0, windowWidth, windowHeight, 0.0, 0.0, 1.0);
	glClear(GL_COLOR_BUFFER_BIT);
	glFlush();
}

void
initTex()
{
	glGenTextures(1, &tex);
}

void
update()
{
	static char *readdata = NULL, *imgdata = NULL;
	static size_t readsize = 0;
	size_t width, height, type, metalen, imgsize,
		   *values[] = { &width, &height, &type };
	char *iter, *end, ndelim;
	int i;

	/* read line from stdin */
	if (getline(&readdata, &readsize, stdin) < -1)
		die("Failed to read input image\n");

	/* parse metadata */
	metalen = strlen(readdata);
	if (metalen >= readsize - 1) die("Metalen format error, no image data\n");

	iter = readdata;
	for (i = 0; i < ARRSIZE(values); i++) {
		end = NULL;
		*values[i] = strtoul(iter, &end, 10);
		ndelim = (i == ARRSIZE(values) - 1) ? '\0' : ',';
		if (!end || *end != ndelim)
			die("Metadata format error, illegal char in %i. metadata entry: %x\n", i + 1, *end);
		iter = end + 1;
	}

	printf("METADATA: %li %li %li\n", width, height, type);

	imgdata = realloc(imgdata, readsize - metalen - 1);
	if (!imgdata) die("OOM!\n");

	base64_decode(readdata + metalen + 1, readsize - metalen - 1, imgdata, &imgsize, 0);
	if (imgsize != width * height * type_bpp[type])
		die("Invalid image size received.. %li != %li\n", imgsize, width * height * type_bpp[type]);

	/* update texture */
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, tex);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, width, height, 0, type_glt[type], GL_UNSIGNED_BYTE, imgdata);
	glBindTexture(GL_TEXTURE_2D, 0);

	/* draw texture */
	glClear(GL_COLOR_BUFFER_BIT);
	glBindTexture(GL_TEXTURE_2D, tex);
	glEnable(GL_TEXTURE_2D);
	glBegin(GL_QUADS);
	glTexCoord2i(0, 0); glVertex2i(0,           0);
	glTexCoord2i(0, 1); glVertex2i(0,           windowHeight);
	glTexCoord2i(1, 1); glVertex2i(windowWidth, windowHeight);
	glTexCoord2i(1, 0); glVertex2i(windowWidth, 0);
	glEnd();
	glDisable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, 0);

	/* display */
	glutSwapBuffers();
}

void
onclick(int button, int e, int x, int y)
{
//	if (button == GLUT_LEFT_BUTTON) {
//		if (e == GLUT_DOWN) {
//		} else {
//		}
//	}
}

void
resize(int w, int h)
{
	windowWidth = w;
	windowHeight = h;
	glViewport(0, 0, w, h);
	initGL();
}

int
main(int argc, char** argv)
{
	// rendering
	initGL();
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGB);
	glutInitWindowSize(windowWidth, windowHeight);
	glutInitWindowPosition(100, 100);
	glutCreateWindow("OpenGL Drawing Demo");
	initGL();
	initTex();
	glutDisplayFunc(update);

	// init timer
	timer(0);

	// controls
	glutMouseFunc(onclick);
	glutReshapeFunc(resize);

	// loop
	glutMainLoop();
	return 0;
}

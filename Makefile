CFLAGS = -I include -I libs/base64/include -g
LDLIBS = -lGL -lglut

BASE64_OBJ = libs/base64/lib/libbase64.o

.PHONY: all clean

all: reader

clean:
	rm reader

$(BASE64_OBJ):
	make -C libs/base64

reader: reader.c $(BASE64_OBJ)
	$(CC) -o $@ $^ $(CFLAGS) $(LDLIBS)
